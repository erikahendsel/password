﻿using System;
using System.Linq;
using Xunit;

namespace Password
{
    public class UnitTest1
    {
        Password password = new Password("Koer123=");
        [Fact]
        public void Check_Password_Length()
        {
            int passwordLength = password.PasswordLength();
            Assert.Equal(8, passwordLength);
        }

        [Fact]
        public void Check_If_Password_Has_an_Upper_Case()
        {
            bool passwordCase = password.ContainsUpperCase();
            Assert.True(passwordCase, "Contains an upper case");
        }

        [Fact]
        public void Check_If_Password_Has_an_Lower_Case()
        {
            bool passwordCase = password.ContainsLowerCase();
            Assert.True(passwordCase, "Contains a lower case");
        }

        [Fact]
        public void Check_If_Password_Contains_A_Number()
        {
            bool passwordHasNumber = password.ContainsANumber();
            Assert.True(passwordHasNumber, "Password contains a number or numbers");
        }

        [Fact]
        public void Check_If_Password_Contains_A_Symbol()
        {
            bool passwordHasNumber = password.ContainsASymbol();
            Assert.True(passwordHasNumber, "Password contains a number or numbers");
        }

        [Fact]
        public void Check_If_Password_Is_Valid()
        {
            bool passwordIsValid = password.PasswordIsValid();
            Assert.True(passwordIsValid, "Password has all of the requirements.");
        }

    }
}

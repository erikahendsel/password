﻿using System;
using System.Linq;
using Xunit;

namespace Password
{
    public class UnitTestPassword
    {
        //[Fact]
        //public void TestMethod1()
        //{
        //}
        [Fact]
        public void Should_BeAbleToAddTwoNumbersTogether()
        {
            //var op = new Password();
            //var result = op.PasswordLength(2, 2);
            //Assert.Equal(4, result);
            var op = new Password();
            var result = op.Exec(1, 2);
            Assert.Equal(3, result);
        }
    }

}

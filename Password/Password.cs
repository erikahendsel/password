﻿using System;
using System.Linq;

namespace Password
{
    public class Password
    {

        public string _password;
        public Password(string password)
        {
            _password = password;
        }
        public int PasswordLength()
        {
            int length = _password.Length;
            if (length >= 6 && length <= 7)
            {
                throw new Exception("Password length is weak. Password needs to contain at least 8 charecters.");
            }
            if (length <= 5 && length >= 1)
            {
                throw new Exception("Password length is very weak. Password needs to contain at least 8 charecters.");
            }
            if(string.IsNullOrEmpty(_password))
            {
                throw new Exception("Password can not be empty. Password needs to contain at least 8 charecters.");
            }
            if (length >= 8)
            {
                Console.WriteLine("Password length is very strong");
            }
            return length;
        }
        public bool ContainsUpperCase()
        {
            return _password.Count(Char.IsUpper) >= 1;
        }
        public bool ContainsLowerCase()
        {
            return _password.Count(Char.IsLower) >= 1;
        }
        public bool ContainsANumber()
        {
            return _password.Count(Char.IsNumber) >= 1;
        }
        public bool ContainsASymbol()
        {
            return _password.Count(Char.IsSymbol) >= 1;
        }
        public bool PasswordIsValid()
        {
            int length = _password.Length;
            bool hasUpperCase = ContainsUpperCase();
            bool hasLowerCase = ContainsLowerCase();
            bool containsANumber = ContainsANumber();
            bool containsASymbol = ContainsASymbol();
            if ( length >= 8 && hasUpperCase && hasLowerCase && containsANumber && containsASymbol)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
